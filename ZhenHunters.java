import java.util.Scanner;

public class ZhenHunters
{
    public static void main(String[] args)
    {

        // Welcome the user to game
        System.out.println("Welcome to the Zhen Hunters game. ");
        Scanner myScanner = new Scanner(System.in);

        System.out.println("Would you like to play the game type y for yes and n for no");
        if(myScanner.next().charAt(0) ==  'y')
        {

            // Created object of GameGrid
            GameGrid game = new GameGrid();
            game.setupGrid();
            game.displayGrid();

            Player player1 = game.getPlayer1();
            Player player2 = game.getPlayer2();

            while (game.getNumZhens() > 0)
            {
                System.out.println("Zhen Player, Select a Zhen to move");
                int x1;
                int y1;
                System.out.println("Zhen Player, Enter x coordinate of a Zhen");
                x1 = myScanner.nextInt();
                System.out.println("Zhen Player, Enter y coordinate of a Zhen");
                y1 = myScanner.nextInt();

                GamePiece selectedZhen = game.getGrid(x1,y1);

                while (!(selectedZhen instanceof Zhen))
                {
                    game.displayGrid();
                    System.out.println("That was not a Zhen, Select again");
                    x1 = myScanner.nextInt();
                    y1 = myScanner.nextInt();
                    selectedZhen = game.getGrid(x1,y1);
                }

                game.displayMovementOptions(2);
                System.out.println("Select a position to move to");
                int newPosition = myScanner.nextInt();
                ((Zhen) selectedZhen).moveToNewPos(newPosition);        // throw exception here
            }

        }

    }

}//ZhenHunters
